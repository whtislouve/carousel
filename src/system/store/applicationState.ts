import { IModalState } from 'app/module/carousel/store';
import { ISystemState } from 'app/system/store/system'

export interface IApplicationState {
  system: ISystemState
  modal: IModalState
}