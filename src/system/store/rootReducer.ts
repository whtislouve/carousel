import { combineReducers } from 'redux'
import { systemReducer } from 'app/system/store/system'
import { modalReducer } from 'app/module/carousel/store'

export const rootReducer = combineReducers({
  system: systemReducer,
  modal: modalReducer,
})