import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { ListPages  } from 'app/system/navigation'
import { Loader } from 'app/module/global/view/Loader'
import { Carousel } from 'app/module/carousel/view/Carousel'
import { MainPage } from 'app/module/mainPage/MainPage'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { CommonTabBar } from 'app/module/global/view'

const Stack = createStackNavigator()
const Tab = createBottomTabNavigator()

// export const RootNavigator = (): JSX.Element => {
//   return (
//     <Stack.Navigator headerMode={undefined}>
//       <Stack.Screen name={ListPages.mainPage} component={MainPage} />
//       <Stack.Screen name={ListPages.Carousel} component={Carousel}/>
//     </Stack.Navigator>
//   )
// }

export const RootNavigator = (): JSX.Element => {
  return(
    <Tab.Navigator 
      tabBar={commonDrawer}
      initialRouteName='MainPage'
    >
      <Tab.Screen name={ListPages.mainPage} component={MainPage} />
      <Tab.Screen  name={ListPages.Carousel} component={Carousel}/>
    </Tab.Navigator>
  )
}


const commonDrawer = (props) => { 
  return(
    <CommonTabBar {...props}/>
  )
}