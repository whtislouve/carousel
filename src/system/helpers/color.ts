export enum Color {
    white = '#FFFFFF',
    darkCharcoal = '#2F2F2F',
    graniteGray = '#626262',
    americanSliver = '#D0D0D0',
    mediumSeaGreen = '#2CB76C',
    royalPurple = '#7A4D9F',
    mazdaBlackMat = '#050505',
    saffron = '#FBC230',
    gainsboro = '#D9D9D9',
    gorse = '#FFE144',
}