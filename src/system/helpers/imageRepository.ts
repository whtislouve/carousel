export class ImageRepository {
//  static readonly loginLogoApplication = require('assets/images/login/logo.png')
    static readonly investGroup = require('assets/images/carousel/investGroup.png')
    static readonly rectangle = require('assets/images/carousel/rectangle.png')
    static readonly close = require('assets/images/carousel/Close.png')
    static readonly mainPage = require('assets/images/mainPage/main.png')
    static readonly products = require('assets/images/mainPage/products.png')
    static readonly knowledge = require('assets/images/mainPage/knowledge.png')
    static readonly favored = require('assets/images/mainPage/favored.png')
    static readonly search = require('assets/images/mainPage/Search.png')
    static readonly sber = require('assets/images/mainPage/sber.png')
    static readonly alfa = require('assets/images/mainPage/alfa.png')
    static readonly bkc = require('assets/images/mainPage/BKC.png')
    
}