export const fonts = {
    geometriaLight: 'Geometria-Light',
    geometriaBold: 'Geometria-Bold',
    openSansRegular: 'OpenSans-Regular',
    latoRegular: 'Lato-Regular',
    latoBold: 'Lato-Bold',
    latoBlack: 'Lato-Black',
    ptsansBold: 'PTSans-Bold',
    ptsansRegular: 'PTSans-Regular',
}