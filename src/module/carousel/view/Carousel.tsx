import React, { PureComponent } from 'react'
import {
  View,
  Text,
  ScrollView,
  Image,
  Alert,
} from 'react-native'
import {
  styleSheetCreate,
  style,
  windowHeight,
  windowWidth,
  ImageRepository,
  fonts,
  Color,
} from 'app/system/helpers'
import {
  CommonInput,
  CustomButton
} from 'app/module/global/view'
import { CommonModal } from 'app/module/global/view'
import { connectStore, IApplicationState } from 'app/system/store'
import { ThunkDispatch } from 'redux-thunk'
import { ModalActions } from '../store'


interface IState {
  activeDot: number
  switchModal: boolean,
}

interface IProps {

}

interface IDispatchProps {
  showModal(data: any): void
}

@connectStore(
  null,
  (dispatch: ThunkDispatch<IApplicationState, void, any>): IDispatchProps => ({
    showModal(data) {
      dispatch(ModalActions.showModalOn(data))
    }
  })
)

export class Carousel extends PureComponent<IProps & IState & IDispatchProps> {
  state = {
    activeDot: 0,
    switchModal: false,
  }

  onScrollHandler = (event: any): void => {
    const { x: positionX } = event.nativeEvent.contentOffset
    const slideSize = event.nativeEvent.layoutMeasurement.width
    const activeDot = Math.round(positionX / slideSize)
    this.setState({ activeDot }, () => {
      console.log(this.state.activeDot)
    })
  }

  toggleMeetModal = () => {
    this.props.showModal({
      title: 'Спасибо!',
      description: 'Приятно познакомиться!'
    })
  }

  toggleStartModal = () => {
    this.props.showModal({
      title: 'Что-то пошло не так!',
      description: 'Попробуйте еще раз'
    })
  }

  render() {
    return (
      <View >
        <ScrollView
          horizontal
          contentContainerStyle={styles.container}
          showsHorizontalScrollIndicator={false}
          decelerationRate="fast"
          pagingEnabled
          onScroll={this.onScrollHandler}
        >
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.slides}>
              <View style={styles.slideOne}>
                <Image
                  source={ImageRepository.investGroup}
                  style={styles.investGroup}
                />
              </View>
              <View style={styles.wordsSlideOneContent}>
                <Text style={styles.wordsSlideOne}>
                  Мы объединили лучшие банковские {'\n'}
              продукты в одном месте{'\n'}
              чтобы вы могли легко и удобно выбрать{'\n'}
              наиболее подходящий вариант
            </Text>
              </View>
            </View>
            <View style={styles.slides}>
              <View style={styles.slideTwo}>
                <Image
                  source={ImageRepository.rectangle}
                  style={styles.rectangleSlideTwo}
                />
              </View>
              <View style={styles.wordsSlideTwoContent}>
                <Text style={styles.wordsSlideOne}>
                  С помошью ползунка выберите комфортную {'\n'}
              сумму и срок инвестирования{'\n'}
              Сравните популярные предложения{'\n'}
              и выберите лучшее для вас
            </Text>
              </View>


            </View>
            <View style={styles.slides}>
              <View style={styles.slideTwo}>
                <Image
                  source={ImageRepository.rectangle}
                  style={styles.rectangleSlideTwo}
                />
              </View>
              <View style={styles.wordsSlideTwoContent}>
                <Text style={styles.wordsSlideOne}>
                  После нажатия на кнопку Подключить  {'\n'}
              оставьте ваши контактные данные на сайте{'\n'}
              выбранного банка
            </Text>
              </View>

            </View>
            <View style={styles.slides}>
              <View style={styles.wordsSlideFourContent}>
                <Text style={styles.wordsSlideOne}>
                  Мы намного быстрее ответим на ваши вопросы  {'\n'}
              и сможем помочь с выбором продукта,{'\n'}
              если будем знакомы
            </Text>
                <Text style={styles.headerFourContent}>
                  Пожалуйста, оставьте ваши контакты
            </Text>
              </View>
              <CommonInput
                placeholder='Введите ваше имя'
                containerStyle={styles.nameInputConatainer}
              />
              <CommonInput
                placeholder='Введите ваш телефон'
                containerStyle={styles.passwordInputContainer}
              />
              <CustomButton
                title='Познакомиться'
                color='WHITE'
                titleStyle={styles.meetButton}
                containerStyle={styles.meetButtonContainer}
                onPress={this.toggleMeetModal}
              />

            </View>

          </View>

        </ScrollView>
        <View style={styles.dotConteiner}>
          <View style={styles.dots}>
            {
              [0, 1, 2, 3].map(item => {
                return (
                  <View
                    key={item}
                    style={this.state.activeDot === item ? styles.dotIsActive : styles.dotInactive}>
                  </View>
                )
              })
            }
          </View>
          <CustomButton
            color='BLACK'
            title={this.state.activeDot === 3 ? 'Начать' : 'Пропустить'}
            containerStyle={styles.buttonSlideFour}
            onPress={this.toggleStartModal}
          />
        </View>
       
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: style.view({
    width: windowWidth * 4,
    height: windowHeight * 0.8,
    //backgroundColor: 'red',
  }),
  slides: style.view({
    width: windowWidth,
    //backgroundColor: 'green',
    alignItems: 'center',

  }),
  slideOne: style.view({
    paddingTop: windowWidth * 0.41,
  }),
  wordsSlideOneContent: style.view({
    paddingTop: windowWidth * 0.216,
  }),
  wordsSlideOne: style.text({
    textAlign: 'center',
    //backgroundColor: 'red',
    fontSize: windowWidth * 0.032,
  }),
  investGroup: style.image({
    width: windowWidth * 0.53,
    height: windowWidth * 0.14,
  }),
  button: style.view({
    marginTop: windowWidth * 0.05
  }),
  slideTwo: style.view({
    paddingTop: windowWidth * 0.2,
  }),
  rectangleSlideTwo: style.image({
    width: windowWidth * 0.71,
    height: windowWidth * 0.7,
  }),
  wordsSlideTwoContent: style.view({
    paddingTop: windowWidth * 0.08,
  }),
  buttonSlideTwo: style.view({
    marginTop: windowWidth * 0.18
  }),
  buttonSlideThree: style.view({
    marginTop: windowWidth * 0.214
  }),
  wordsSlideFourContent: style.view({
    paddingTop: windowWidth * 0.37
  }),
  headerFourContent: style.text({
    fontSize: windowWidth * 0.045,
    fontFamily: fonts.geometriaBold,
    paddingTop: windowWidth * 0.11,
  }),
  nameInputConatainer: style.view({
    marginTop: windowWidth * 0.07,
  }),
  passwordInputContainer: style.view({
    marginTop: windowWidth * 0.02,
  }),
  meetButtonContainer: style.view({
    marginTop: windowWidth * 0.02,
    width: windowWidth * 0.75,
    borderWidth: windowWidth * 0.001
  }),
  meetButton: style.text({
    color: Color.mazdaBlackMat,
    fontFamily: fonts.geometriaBold
  }),
  buttonSlideFour: style.view({
    marginTop: windowWidth * 0.09,
    alignItems: 'center'
  }),
  dotInactive: style.view({
    width: windowWidth * 0.02,
    height: windowWidth * 0.02,
    borderRadius: windowWidth * 0.375,
    backgroundColor: Color.gainsboro,
    marginRight: windowWidth * 0.02,
  }),
  dotIsActive: style.view({
    width: windowWidth * 0.02,
    height: windowWidth * 0.02,
    borderRadius: windowWidth * 0.375,
    backgroundColor: Color.saffron,
    marginRight: windowWidth * 0.02,
  }),
  dotConteiner: style.view({
    height: windowWidth * 0.33,
    flexDirection: 'column',
    //backgroundColor: 'yellow',
    alignItems: 'center'
  }),
  dots: style.view({
    flexDirection: 'row'
  }),
})