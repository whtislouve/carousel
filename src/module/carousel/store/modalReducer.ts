import { ReducerBuilder, reducerWithInitialState } from "typescript-fsa-reducers"
import { IModalInitialState, IModalState } from "./modalState"
import { ModalActions } from './modalActions'

const modalOn = (state: IModalState, payload: any): IModalState => {
    return {
        ...state,
        isVisible: true,
        title: payload.title,
        description: payload.description,
    }
}

const modalOff = (state: IModalState):IModalState => {
    return {
        ...state,
        isVisible: false,
    }
}

export const modalReducer: ReducerBuilder<IModalState> = reducerWithInitialState(IModalInitialState)
    .case(ModalActions.showModalOn, modalOn)
    .case(ModalActions.showModalOff, modalOff)