import { actionCreator } from 'app/system/store/actionCreator'

export class ModalActions {
    static showModalOn = actionCreator<{title: string, description: string}>('MODAL/MODAL_ON')
    static showModalOff = actionCreator('MODAL/MODAL_OFF')
}