export interface IModalState {
    isVisible: boolean
    title: string
    description: string
}

export const IModalInitialState: IModalState = {
    isVisible: false,
    title: '',
    description: '',
}