import React, { PureComponent } from 'react'
import {
  style,
  styleSheetCreate,
  windowWidth,
  ImageRepository,
  Color,
  fonts,
} from 'app/system/helpers'
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,
  Alert,
  ImageURISource,
} from 'react-native'

interface IProps {
  navigation: any
}

interface IState {

}

interface IMenu {
  title: string
  image: ImageURISource
  route: string
  titleStyle: any
  containerStyle: any
}

export class CommonTabBar extends PureComponent<IProps, IState> {
  render() {
    const {
      navigation,
    } = this.props
    return (
      <View style={styles.tabBarContainer}>
        {
          tabBarList.map(items => {
            return (
              <TouchableOpacity
                onPress={() => { navigation.navigate(items.route) }}
                style={items.containerStyle}>
                <View style={styles.mainPageElements}>
                  <Image
                    style={styles.mainPage}
                    source={items.image}
                  />
                  <Text style={items.titleStyle}>
                    {items.title}
                  </Text>
                </View>
              </TouchableOpacity>
            )
          })
        }
      </View>
    )
  }
}

const styles = styleSheetCreate({
  tabBarContainer: style.view({
    position: 'absolute',
    bottom: 0,
    width: '100%',
    //height: windowWidth * 0.192,
    backgroundColor: Color.mazdaBlackMat,
    flexDirection: 'row',
  }),
  mainPageContainer: style.view({
    width: windowWidth * 0.25,
    height: windowWidth * 0.192,
    backgroundColor: Color.gorse,
    borderTopLeftRadius: windowWidth * 0.02,
    borderTopRightRadius: windowWidth * 0.02,
    alignItems: 'center',
    justifyContent: 'center',
  }),
  mainPage: style.view({
    width: windowWidth * 0.064,
    height: windowWidth * 0.064,
  }),
  mainPageText: style.text({
    fontSize: windowWidth * 0.029,
  }),
  mainPageElements: style.view({
    alignItems: 'center',
  }),
  productsContainer: style.view({
    width: windowWidth * 0.25,
    height: windowWidth * 0.192,
    alignItems: 'center',
    justifyContent: 'center',
  }),
  productsText: style.text({
    fontSize: windowWidth * 0.029,
    color: Color.white,
  }),
})


const tabBarList: IMenu[] = [
  {
    title: 'Главная',
    image: ImageRepository.mainPage,
    route: 'MainPage',
    titleStyle: styles.mainPageText,
    containerStyle: styles.mainPageContainer,
  },
  {
    title: 'Продукты',
    image: ImageRepository.products,
    route: 'Carousel',
    titleStyle: styles.productsText,
    containerStyle: styles.productsContainer,
  },
  {
    title: 'Знания',
    image: ImageRepository.knowledge,
    route: 'qqq',
    titleStyle: styles.productsText,
    containerStyle: styles.productsContainer,
  },
  {
    title: 'Избранное',
    image: ImageRepository.favored,
    route: 'qqq',
    titleStyle: styles.productsText,
    containerStyle: styles.productsContainer,
  },
]