import React, { PureComponent } from 'react'
import {
  View,
  TextInput,
  TextInputProps,
  TextStyle,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native'
import {
  styleSheetCreate,
  style,
  Color,
  fonts,
  windowHeight,
  windowWidth,
  platform,
  styleSheetFlatten,
  ImageRepository,
} from 'app/system/helpers'

interface IProps extends TextInputProps {
  containerStyle?: TextStyle
  inputStyle?: TextStyle
}

interface IState {

}

export class CommonInputSearch extends PureComponent<IProps, IState>{
  render() {
    const {
      containerStyle,
      inputStyle,
    } = this.props

    const inputContainer = styleSheetFlatten([
      styles.inputContainer,
      containerStyle,
    ])

    const textInput = styleSheetFlatten([
      styles.input,
      inputStyle
    ])

    return (
      <View style={inputContainer}>
        <Image
          source={ImageRepository.search}
          style={styles.search}
        />
        <TextInput
          {...this.props}
          style={textInput}
        />
        <Image
          source={ImageRepository.close}
          style={styles.close}
        />
      </View>
    )
  }
}

const styles = styleSheetCreate({
  inputContainer: style.view({
    backgroundColor: Color.white,
    width: windowWidth * 0.91,
    borderRadius: windowWidth * 0.13,
    flexDirection: 'row',
    alignItems: 'center'
  }),
  input: style.text({
    height: windowWidth * 0.12,
    width: windowWidth * 0.7,
    fontSize: windowWidth * 0.039,

  }),
  search: style.image({
    width: windowWidth * 0.08,
    height: windowWidth * 0.08,
    marginLeft: windowWidth * 0.032,
  }),
  close: style.image({
    width: windowWidth * 0.08,
    height: windowWidth * 0.08,
    marginRight: windowWidth * 0.032,
  }),
})
