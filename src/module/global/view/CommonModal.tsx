import React, { PureComponent } from 'react'
import {
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native'
import {
  fonts,
  style,
  windowWidth,
  windowHeight,
  styleSheetCreate,
  ImageRepository,
  Color,
} from 'app/system/helpers'
import Modal from 'react-native-modal'
import { CustomButton } from 'app/module/global/view'
import { connectStore, IApplicationState } from 'app/system/store'
import { ThunkDispatch } from 'redux-thunk'
import { ModalActions } from 'app/module/carousel/store'


interface IProps {
  title?: string
  description?: string
  switchModal?: any
}

interface IState {

}

interface IStateProps {
  isVisible: boolean
  title: string
  description: string
}

interface IDispatchProps {
  hideModal(): void
}

@connectStore(
  (state: IApplicationState): IStateProps => ({
    isVisible: state.modal.isVisible,
    title: state.modal.title,
    description: state.modal.description,
  }),
  (dispatch: ThunkDispatch<IApplicationState, void, any>): IDispatchProps => ({
    hideModal() {
      dispatch(ModalActions.showModalOff())
    }
  })
)

export class CommonModal extends PureComponent<IProps & IState & IDispatchProps & IStateProps> {
  render() {
    const {
      title,
      description,
      switchModal,
    } = this.props
    return (
      <Modal
        isVisible={this.props.isVisible}
      >
        <View style={styles.container}>
          <View style={styles.closeImageConteiner}>
            <TouchableOpacity onPress={this.props.hideModal}>
              <Image
                source={ImageRepository.close}
                style={styles.closeImage}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.titleContainer}>
            <View style={styles.headerContent}>
              <Text style={styles.headerText}>
                {title}
              </Text>
            </View>
            <View style={styles.mainContent}>
              <Text style={styles.mainContentText}>
                {description}
              </Text>
            </View>
            <CustomButton
              title='Ок'
              color='BLACK'
              containerStyle={styles.button}
            />
          </View>
        </View>
      </Modal>
    )
  }
}

const styles = styleSheetCreate({
  container: style.view({
    right: windowWidth * 0.036,
    width: windowWidth * 0.97,
    height: windowWidth * 0.59,
    backgroundColor: Color.white,
    borderRadius: windowWidth * 0.02
  }),
  closeImageConteiner: style.view({
    alignItems: 'flex-end',
    //backgroundColor: 'red',
    marginTop: windowWidth * 0.028,
  }),
  closeImage: style.image({
    width: windowWidth * 0.06,
    height: windowWidth * 0.06,
    marginRight: windowWidth * 0.028,
  }),
  headerText: style.text({
    fontSize: windowWidth * 0.056,
    fontFamily: fonts.geometriaBold
  }),
  headerContent: style.view({
    paddingTop: windowWidth * 0.028,
    //backgroundColor: 'green',
  }),
  mainContent: style.view({
    paddingTop: windowWidth * 0.02,
    //backgroundColor: 'green',
  }),
  mainContentText: style.text({
    fontSize: windowWidth * 0.048,
  }),
  button: style.view({
    width: windowWidth * 0.22,
    marginTop: windowWidth * 0.05,
  }),
  titleContainer: style.view({
    alignItems: 'center'
  }),
})