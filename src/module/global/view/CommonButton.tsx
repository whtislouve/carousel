import React, { PureComponent } from 'react'
import { 
  Text,
  TextStyle,
  TouchableOpacity, 
  TouchableOpacityProps, 
} from 'react-native'
import {
  styleSheetCreate,
  style,
  styleSheetFlatten,
  windowWidth,
  Color,
  fonts,
} from 'app/system/helpers'

interface IProps extends TouchableOpacityProps {
  title?: string
  color?: 'BLACK' | 'PURPLE' | 'WHITE'
  titleStyle?: TextStyle
  containerStyle?: TextStyle
}

interface IState {

}

const buttonColors = {
  BLACK: Color.mazdaBlackMat,
  PURPLE: Color.royalPurple,
  WHITE: Color.white,
}

export class CustomButton extends PureComponent<IProps,IState>{
  render() {
    const {
      title,
      color,
      disabled,
      titleStyle,
      containerStyle,
    } = this.props

    const button = styleSheetFlatten([
      styles.button,
      containerStyle,
      {
        backgroundColor: disabled ? Color.darkCharcoal : color ? buttonColors[color] : Color.mediumSeaGreen
      },
      
    ])

    const customTitle = styleSheetFlatten([
      styles.text,
      titleStyle,
    ])

    return (
      <TouchableOpacity
        {...this.props}
        style={button}
      >
        <Text style={customTitle}>
          {title}
        </Text>
      </TouchableOpacity>
    )
  }
}

const styles = styleSheetCreate({
  button: style.view({
    width: windowWidth * 0.42,
    height: windowWidth * 0.11,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: windowWidth * 0.021,
  }),
  text: style.text({
    fontSize: windowWidth * 0.032,
    fontFamily: fonts.latoRegular,
    color: Color.white,
  }),
})

