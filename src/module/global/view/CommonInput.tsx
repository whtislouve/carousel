import React, { PureComponent } from 'react'
import {
  View,
  TextInput,
  TextInputProps,
  TextStyle,
  Text,
} from 'react-native'
import {
  styleSheetCreate,
  style,
  Color,
  fonts,
  windowHeight,
  windowWidth,
  platform,
  styleSheetFlatten,
} from 'app/system/helpers'

interface IProps extends TextInputProps {
  containerStyle?: TextStyle
  inputStyle?: TextStyle
}

interface IState {

}

export class CommonInput extends PureComponent<IProps, IState>{
  render() {
    const {
      containerStyle,
      inputStyle,
    } = this.props

    const inputContainer = styleSheetFlatten([
      styles.inputContainer,
      containerStyle,
    ])

    const textInput = styleSheetFlatten([
      styles.input,
      inputStyle
    ])

    return (
      <View style={inputContainer}>
        <TextInput
          {...this.props}
          style={textInput}
        />
      </View>
    )
  }
}

const styles = styleSheetCreate({
  inputContainer: style.view({
    backgroundColor: Color.white,
    width: windowWidth * 0.91,
    borderRadius: windowWidth * 0.021,
  }),
  input: style.text({
    height: windowWidth * 0.13,
    fontSize: windowWidth * 0.039,
    paddingLeft: windowWidth * 0.05,
    paddingRight: windowWidth * 0.04,
  }),
})
