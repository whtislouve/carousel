import React, { PureComponent } from 'react'
import {
  View,
  Text,
  TextInputProps,
  TextStyle,
  Image,
} from 'react-native'
import {
  style,
  styleSheetCreate,
  styleSheetFlatten,
  windowWidth,
  fonts,
  Color,
} from 'app/system/helpers'
import { weekdaysMin } from 'moment'


interface IState {

}

interface IProps extends TextInputProps {
  containerStyle?: TextStyle
  bankImage?: any
  nameProduct?: string
  description?: string
  percent?: number
  percentPrice?: number
}

export class CommonCard extends PureComponent<IProps, IState> {
  render() {
    const {
      containerStyle,
      nameProduct,
      description,
      percent,
      percentPrice,
      bankImage,
    } = this.props

    const cardContainer = styleSheetFlatten([
      styles.container,
      containerStyle,
      
    ])
    return (
      <View>
        <View style={cardContainer}>
        <Image
          source={bankImage}
          style={styles.bankLogo}
        />
        <View style={styles.content}>
          <Text style={styles.title}>
            {nameProduct}
          </Text>
          <Text style={styles.description}>
            {description}
          </Text>
        </View>
        <View style={styles.offers}>
          <View style={styles.percentContainer}>
            <Text style={styles.percent}>
              до {percent} %
            </Text>
            <Text style={styles.procentPrice}>
              до {percentPrice} ₽
            </Text>
          </View>
        </View>
      </View>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: style.view({
    backgroundColor: Color.white,
    width: windowWidth * 0.91,
    height: windowWidth * 0.23,
    borderRadius: windowWidth * 0.042,
    flexDirection: 'row',
    paddingTop: windowWidth * 0.04,
    marginTop: windowWidth * 0.02,
    paddingLeft: windowWidth * 0.042,
  }),
  bankLogo: style.view({
    width: windowWidth * 0.149,
    height: windowWidth * 0.149,
  }),
  title: style.text({
    fontSize: windowWidth * 0.04,
    fontFamily: fonts.latoBold,
    paddingTop: windowWidth * 0.021,
  }),
  description: style.text({
    fontSize: windowWidth * 0.032,
    fontFamily: fonts.ptsansRegular,
    paddingTop: windowWidth * 0.021
  }),
  percentContainer: style.view({
    width: windowWidth * 0.232,
    height: windowWidth * 0.085,
    backgroundColor: Color.gorse,
    borderRadius: windowWidth * 0.13,
    alignItems: 'center',
  }),
  percent: style.text({
    paddingTop: windowWidth * 0.016,
    fontSize: windowWidth * 0.04
  }),
  procentPrice: style.text({
    paddingTop: windowWidth * 0.02,
    fontSize: windowWidth * 0.032,
  }),
  content: style.view({
    paddingLeft: windowWidth * 0.042,
  }),
  offers: style.view({
    position: 'absolute', 
    right: windowWidth * 0.037, 
    top: windowWidth * 0.05
  })
})