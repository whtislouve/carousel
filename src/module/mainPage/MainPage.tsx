import React, { PureComponent } from 'react'
import {
  style,
  styleSheetCreate,
  windowWidth,
  ImageRepository,
  Color,
  fonts,
} from 'app/system/helpers'
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,
  Alert,
} from 'react-native'
import {
  CommonInputSearch,
  CommonCard,
  CommonDrawer,
} from 'app/module/global/view'
import Slider from "react-native-slider"

interface IProps {
  navigation: any
}

interface IState {
  sliderValue: number
  sliderYearValue: number
  search: string
  yearValue: string
}

const cards = [
  {
    nameProduct: 'Сбербанк Инвестор',
    procent: 23,
    minimalAmount: 1000,
    bankImage: ImageRepository.sber
  },
  {
    nameProduct: 'Альфа-Директ',
    procent: 23,
    minimalAmount: 1000,
    bankImage: ImageRepository.alfa
  },
  {
    nameProduct: 'БКС',
    procent: 23,
    minimalAmount: 1000,
    bankImage: ImageRepository.bkc
  }
]

export class MainPage extends PureComponent<IProps, IState> {
  state = {
    sliderValue: 1000,
    sliderYearValue: 1,
    search: '',
    yearValue: '1 мес.'
  }

  onChangeSearchValue = (search: string) => {
    this.setState({ search })
  }

  onChangeInvestValue = (sliderValue: number) => {
    this.setState({ sliderValue: sliderValue - 1000 })
  }

  countYears() {
    let i
    if (this.state.sliderYearValue / 12 == 1){
      i = this.state.sliderYearValue / 12 + ' год'
      this.setState({ yearValue: i })
    }
    else if (this.state.sliderYearValue / 12 > 1 && this.state.sliderYearValue / 12 <= 4  && this.state.sliderYearValue % 12 == 0) {
      i = this.state.sliderYearValue / 12 + ' года'
      this.setState({ yearValue: i })
    }
    else if (this.state.sliderYearValue / 12 >= 5 && this.state.sliderYearValue % 12 == 0){
      i = this.state.sliderYearValue / 12 + ' лет'
      this.setState({ yearValue: i })
    }
    else {
      this.setState({ yearValue: this.state.sliderYearValue.toString() + ' мес.' })
    }
  }
  onChangeYearValue = (sliderYearValue: number) => {
    this.setState({ sliderYearValue }, this.countYears)
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <CommonInputSearch
            placeholder='Поиск'
            onChangeText={this.onChangeSearchValue}
            value={this.state.search}
          />
          <View style={styles.investConteiner}>
            <View style={styles.investContent}>
              <Text style={styles.wantInvest}>
                Хочу инвестировать
              </Text>
              <Text>
                {this.state.sliderValue.toString().replace(/(\d)(?=(\d{3})+([^\d]|$))/g, "$1 ")} ₽
              </Text>
              <Slider
                value={this.state.sliderValue}
                minimumValue={1000}
                maximumValue={10000000}
                step={10000}
                onValueChange={this.onChangeInvestValue}
                style={styles.wantInvestSlider}
                thumbTintColor={Color.gorse}
                minimumTrackTintColor={Color.gorse}
              />
              <View style={styles.sliderAmount}>
                <Text style={styles.sliderText}>
                  1 000 ₽
                </Text>
                <Text>
                  10 000 000 ₽
                </Text>
              </View>
              <Text style={styles.timeInvest}>
                Срок инвестирования
              </Text>
              <Text>
                {this.state.yearValue}            
              </Text>
              <Slider
                value={this.state.sliderYearValue}
                minimumValue={1}
                maximumValue={120}
                step={1}
                onValueChange={this.onChangeYearValue}
                style={styles.wantInvestSlider}
                thumbTintColor={Color.gorse}
                minimumTrackTintColor={Color.gorse}
              />
              <View style={styles.sliderAmount}>
                <Text style={styles.sliderTimeText}>
                  1 мес.
                </Text>
                <Text>
                  10 лет
                </Text>
              </View>
            </View>
          </View>
          <Text style={styles.bestOffers}>
            Лучшие предложения
          </Text>
          {
            cards
              .filter((item) => item.nameProduct.toLocaleLowerCase().includes(this.state.search.toLocaleLowerCase()))
              .map((item) => {
                return (
                  <CommonCard
                    nameProduct={item.nameProduct}
                    description='Минимальная сумма'
                    percent={item.procent}
                    percentPrice={item.minimalAmount}
                    bankImage={item.bankImage}
                  />
                )
              })
          }
        </ScrollView>
      </View>
    )
  }
}


const styles = styleSheetCreate({
  container: style.view({
    height: '100%',
    alignItems: 'center',
    paddingTop: windowWidth * 0.08,
    paddingBottom: windowWidth * 0.23,
  }),
  
  investConteiner: style.view({
    width: windowWidth * 0.91,
    height: windowWidth * 0.74,
    backgroundColor: Color.white,
    marginTop: windowWidth * 0.064,
    borderRadius: windowWidth * 0.042,
  }),
  investContent: style.view({
    paddingHorizontal: windowWidth * 0.064,
    paddingTop: windowWidth * 0.08,
    alignItems: 'center'
  }),
  wantInvest: style.text({
    fontSize: windowWidth * 0.048,
    fontFamily: fonts.geometriaBold
  }),
  wantInvestSlider: style.view({
    width: windowWidth * 0.848,
  }),
  sliderAmount: style.view({
    flexDirection: 'row',
  }),
  sliderText: style.text({
    paddingRight: windowWidth * 0.52,
  }),
  timeInvest: style.text({
    paddingTop: windowWidth * 0.05,
    fontSize: windowWidth * 0.048,
    fontFamily: fonts.geometriaBold
  }),
  sliderTimeText: style.text({
    paddingRight: windowWidth * 0.65,
  }),
  bestOffers: style.text({
    fontSize: windowWidth * 0.048,
    marginTop: windowWidth * 0.088,
    marginBottom: windowWidth * 0.02
  }),
})